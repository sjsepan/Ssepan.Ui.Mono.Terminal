﻿using System;
using System.Diagnostics;
using System.IO;
using Ssepan.Utility.Mono;
using Ssepan.Io.Mono;
using Ssepan.Application.Mono;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made redistributable by Microsoft
using System.Reflection;

namespace Ssepan.Ui.Mono.Terminal
{
    public class Dialogs
    {
        #region Declarations
        public const Char FilterSeparator = '|';
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. 
        /// </summary>
        /// <param name="fileDialogInfo"></param>
        /// <param name="errorMessage"></param>
        public static bool GetPathForSave
        (
            ref FileDialogInfo<object, string> fileDialogInfo, 
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string messageTemp = null;
            string messageResponse = null;
            string fileResponse = null;
            string currentPath = null;
            string[] nameAndPattern = null;

            try
            {
                if 
                (
                    (fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)) 
                    || 
                    (fileDialogInfo.ForceDialog)
                )
                {
                    Console.WriteLine((fileDialogInfo.ForceDialog ? "Save As..." : "Save..."));
                    Console.WriteLine("Current filename: ", fileDialogInfo.Filename);
                    Console.WriteLine("Filters...");
                    foreach (string filter in fileDialogInfo.Filters.Split(FilterSeparator))
                    {
                        nameAndPattern = filter.Split(FilterSeparator);
                        Console.WriteLine("Name: ", nameAndPattern[0], ", Pattern: ", nameAndPattern[1]);
                    }
                    currentPath = 
                    (
                        fileDialogInfo.InitialDirectory == default(Environment.SpecialFolder) 
                        ? 
                        fileDialogInfo.CustomInitialDirectory 
                        : 
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    );
                    Console.WriteLine("Current folder: ", currentPath);

                    Console.Write("Filename?: ");
                    fileResponse = Console.ReadLine();

                    if (!string.IsNullOrWhiteSpace(fileResponse))
                    {
                        if (Path.GetFileName(fileResponse).ToLower() == (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower())
                        {
                            //user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
                            messageTemp = "The name \"" + (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower() + "\" is not allowed; please choose another. Settings not saved.";
                            MessageDialogInfo<object, string, object, string, object> messageDialogInfo = 
                                new MessageDialogInfo<object, string, object, string, object>
                                (
                                    fileDialogInfo.Parent,
                                    fileDialogInfo.Modal,
                                    fileDialogInfo.Title,
                                    null,
                                    "Info",
                                    null,
                                    messageTemp,
                                    messageResponse
                                );
                            ShowMessageDialog
                            (
                                ref messageDialogInfo,
                                ref errorMessage
                            ); 
                        }
                        else
                        {
                            //set new filename
                            fileDialogInfo.Filename = fileResponse;
                            fileDialogInfo.Filenames = new string[] {fileResponse};
                        }
                        fileDialogInfo.Response = fileResponse;
                        returnValue = true;
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. 
        /// </summary>
        /// <param name="fileDialogInfo"></param>
        /// <param name="errorMessage"></param>
        public static bool GetPathForLoad
        (
            ref FileDialogInfo<object, string> fileDialogInfo, 
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string messageTemp = null;
            string messageResponse = null;
            string fileResponse = null;
            string currentPath = null;
            string[] nameAndPattern = null;

            try
            {
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
                    string PreviousFileName = fileDialogInfo.Filename;

                    //define location of file for settings by prompting user for filename.
                    Console.WriteLine("Open...");
                    Console.WriteLine("Current filename: ", fileDialogInfo.Filename);
                    Console.WriteLine("Filters...");
                    foreach (string filter in fileDialogInfo.Filters.Split(FilterSeparator))
                    {
                        nameAndPattern = filter.Split(FilterSeparator);
                        Console.WriteLine("Name: ", nameAndPattern[0], ", Pattern: ", nameAndPattern[1]);
                    }
                    currentPath = 
                    (
                        fileDialogInfo.InitialDirectory == default(Environment.SpecialFolder) 
                        ? 
                        fileDialogInfo.CustomInitialDirectory 
                        : 
                        Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    );
                    Console.WriteLine("Current folder: ", currentPath);

                    Console.Write("Filename?: ");
                    fileResponse = Console.ReadLine();

                    if (!string.IsNullOrWhiteSpace(fileResponse))
                    {
                        if (Path.GetFileName(fileResponse).ToLower() == (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower())
                        {
                            //user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
                            messageTemp = "The name \"" + (fileDialogInfo.NewFilename + "." + fileDialogInfo.Extension).ToLower() + "\" is not allowed; please choose another. Settings not opened.";
                            MessageDialogInfo<object, string, object, string, object> messageDialogInfo = 
                                new MessageDialogInfo<object, string, object, string, object>
                                (
                                    fileDialogInfo.Parent,
                                    fileDialogInfo.Modal,
                                    fileDialogInfo.Title,
                                    null,
                                    "Info",
                                    null,
                                    messageTemp,
                                    messageResponse
                                );
                            ShowMessageDialog
                            (
                                ref messageDialogInfo,
                                ref errorMessage
                            ); 
                        }
                        else
                        {
                            //set new filename
                            fileDialogInfo.Filename = fileResponse;
                            fileDialogInfo.Filenames = new string[] {fileResponse};
                        }
                        fileDialogInfo.Response = fileResponse;
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. 
        /// </summary>
        /// <param name="fileDialogInfo">returns path in Filename property</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool GetFolderPath
        (
            ref FileDialogInfo<object, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string fileResponse = null;
            string currentPath = null;

            try
            {
                Console.WriteLine("Open folder...");
                currentPath = 
                (
                    fileDialogInfo.InitialDirectory == default(Environment.SpecialFolder) 
                    ? 
                    fileDialogInfo.CustomInitialDirectory 
                    : 
                    Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                );
                Console.WriteLine("Current folder: ", currentPath);

                Console.Write("Folder?: ");
                fileResponse = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(fileResponse))
                {
                    fileDialogInfo.Filename = fileResponse;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static. 
        /// </summary>
        /// <param name="aboutDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool ShowAboutDialog
        (
            ref AboutDialogInfo<object, string, object> aboutDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string response = null;

            try
            {
                Console.WriteLine(aboutDialogInfo.Title,": ");
                Console.WriteLine("ProgramName: ", aboutDialogInfo.ProgramName);
                Console.WriteLine("Version: : ", aboutDialogInfo.Version);
                Console.WriteLine("Copyright: ", aboutDialogInfo.Copyright);
                Console.WriteLine("Comments: : ", aboutDialogInfo.Comments);
                Console.WriteLine("Website: : ", aboutDialogInfo.Website);
                Console.Write("Press ENTER to Continue...");
                response = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(response))
                {
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static. 
        /// </summary>
        /// <param name="printerDialogInfo">PrinterDialogInfo</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<object, string, string> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string printerResponse = null;
            
            try
            {
                Console.Write(printerDialogInfo.Title,": ");
                //TODO:list available printers
                printerResponse = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(printerResponse))
                {
                    printerDialogInfo.Name = printerResponse;
                    printerDialogInfo.Response = "OK";
                    returnValue = true;
                }
                else
                {
                    printerDialogInfo.Response = "Cancel";
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static. 
        /// </summary>
        /// <param name="messageDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool ShowMessageDialog
        (
            ref MessageDialogInfo<object, string, object, string, object> messageDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string response = null;

            try
            {
                Console.Write(messageDialogInfo.MessageType, "; ", messageDialogInfo.Title,": ");
                response = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(response))
                {
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

            }

            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// Static. 
        /// </summary>
        /// <param name="colorDialogInfo">ColorDialogInfo</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<object, string, string> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string response = null;

            try
            {
                Console.Write(colorDialogInfo.Title,": ");
                response = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(response))
                {
                    colorDialogInfo.Color = response;
                    colorDialogInfo.Response = response;
                    returnValue = true;
                }
            
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static. 
        /// </summary>
        /// <param name="fontDialogInfo"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool GetFont
        (
            ref FontDialogInfo<object, string, string, string> fontDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string response = null;

            try
            {

                Console.Write(fontDialogInfo.Title,": ");
                response = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(response))
                {
                    fontDialogInfo.FontDescription = response;
                    fontDialogInfo.Response = response;
                    returnValue = true;
                }
            
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return returnValue;
        }


        // /// <summary>
        // /// Perform input of connection string and provider name.
        // /// Uses MS Data Connections Dialog.
        // /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        // /// </summary>
        // /// <param name="connectionString"></param>
        // /// <param name="providerName"></param>
        // /// <param name="errorMessage"></param>
        // /// <returns></returns>
        // public static bool GetDataConnection
        // (
        //     ref string connectionString,
        //     ref string providerName,
        //     ref string errorMessage
        // )
        // {
        //     bool returnValue = default(bool);
        //     //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
        //     //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

        //     try
        //     {
        //         //dataConnectionDialog = new DataConnectionDialog();

        //         //DataSource.AddStandardDataSources(dataConnectionDialog);

        //         //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
        //         //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

        //         //dataConnectionConfiguration = new DataConnectionConfiguration(null);
        //         //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

        //         //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data COnenction Dialog.
        //         //dataConnectionDialog.ConnectionString = connectionString;

        //         if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
        //         {
        //             ////extract connection string
        //             //connectionString = dataConnectionDialog.ConnectionString;
        //             //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

        //             ////writes provider selection to xml file
        //             //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

        //             ////save these too
        //             //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
        //             //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

        //             returnValue = true;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         errorMessage = ex.Message;
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //     }
        //     return returnValue;
        // }
        #endregion Methods
    }
}
