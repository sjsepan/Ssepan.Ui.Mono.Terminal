# readme.md - README for Ssepan.Ui.Mono.Terminal v6.0

## About

Common library of application functions for C# Mono applications, specific to console UIs; requires ssepan.application.mono, ssepan.io.mono, ssepan.utility.mono

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~initial release
~Due to Mono / XBuild issues with nested namespaces, I created this lib as Ssepan.Ui.Mono.Terminal instead of Ssepan.Application.Mono.Terminal, which would have been in line with the naming of the .Net Core projects. Consider this lib the Mono equivalent of Ssepan.Application.Core.Terminal.

Steve Sepan
ssepanus@yahoo.com
6/1/2022
